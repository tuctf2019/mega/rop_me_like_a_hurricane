# rop me like a hurricane

Desc: `Didn't get an alien in Area51, but I found this...`

Prereq: `Broken`

Hints:

* Is there an order to the madness?

Flag: `TUCTF{bu7_c4n_y0u_ROP_bl1ndf0ld3d?}`
