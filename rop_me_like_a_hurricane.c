#include <stdio.h>  // prints
#include <stdlib.h> // malloc
#include <string.h> // strcmp
#include <unistd.h> // read
#include <fcntl.h>  // open
#include <unistd.h> // close
#include <time.h>   // time

// "hidden" (unused) string for the remote explot of this challenge
char *remote = "nc chal.tuctf.com 31058";

#define FLAG_LEN 64

// 3 booleans to be set
int a = 0;
int b = 0;
int c = 0;

// 3
void A() {
	if (c && b) a = 1;
}

// 1
void B(int v) {
	b = 1;
}

// 2
void C() {
	if (b) c = 1;
}

void printFlag() {
	char *flag = malloc(64);
	int fd = open("./flag.txt", 0);

	if (a && b && c) {
		read(fd, flag, FLAG_LEN);
		printf("%s\n", flag);
	}

	close(fd);
}

void pwnme() {
	printf("You got this!\n> ");
	char buf[16];
	read(0, buf, 64);
}

int main() {
    setvbuf(stdout, NULL, _IONBF, 20);
    setvbuf(stdin, NULL, _IONBF, 20);

	pwnme();

    return 0;
}
